FROM node:8.14.0-alpine AS Base

RUN mkdir -p /app /app/uploads

WORKDIR /app

COPY  ./ /app

RUN npm cache clean -f && npm i

RUN npm run prod

FROM Base as Dev

ENV NODE_ENV=development

EXPOSE 3555

CMD npm run dev


FROM keymetrics/pm2:latest-alpine as Prod

WORKDIR /app

ENV NODE_ENV=production

COPY --from=Base ./app/ .

EXPOSE 3555
CMD ["pm2-docker", "start", "--json", "./.build/main.js"]