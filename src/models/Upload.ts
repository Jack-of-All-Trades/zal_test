const _ = require('lodash'),
      fs = require('fs'),
      helper = require('../../config/helper').root

import {IFile} from '../interfaces/IFile';

export class Upload {
    // add file to the system.
    static async UploadFile(data: IFile): Promise<string> {
        try {
            if (await fs.existsSync(helper(`uploads/${data.firstname}_${data.lastname}.txt`))) {
                return "User with same first name and last name exist"
            } 
            await fs.writeFile(helper(`uploads/${data.firstname}_${data.lastname}.txt`), JSON.stringify(data), (err) => {
                if(err) {
                throw err;
                }
            })
          return "Successfully added data into the system";
        } catch(e) {
            console.log(e);
            throw new Error(e);
        }
    }

    // delete file from the system.
    static async DeleteFile(data: string): Promise<string> {
        try {
            if (await fs.existsSync(helper(`uploads/${data}.txt`))) {
                await fs.unlink(helper(`uploads/${data}.txt`), (err) => {
                    if (err) throw err
                });
                return "Successfully deleted file";
            } 
            return "File does not exist";
        } catch(e) {
            console.log(e);
            throw new Error(e);
        }
    }
}