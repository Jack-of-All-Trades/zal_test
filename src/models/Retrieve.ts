const _ = require('lodash'),
      fs = require('fs'),
      helper = require('../../config/helper').root
import {IFile} from '../interfaces/IFile';

export class Retrieve {
    //get user related file.
    static async GetOne(data: string): Promise<Object> {
        try {
            let check: IFile = await fs.readFileSync(helper(`uploads/${data}.txt`));
            return check;
        } catch (e) {
            console.log(e);
            throw new Error(e);
        }
    }
}