import { JsonController, Res, Param, Post, Body, OnUndefined, Delete} from 'routing-controllers';
import {Upload} from '../models/Upload';

@JsonController('/upload')

export class UploadController {

    @Post('/')
    @OnUndefined(404)
    async CreateFile(@Body() data, @Res() response: any ) {
        try {
            let createfile = await Upload.UploadFile(data);
            return createfile;
        } catch(e) {
            console.log(e);
            throw new Error(e);       
        }
    }

    @Delete('/:data')
    @OnUndefined(404)
    async RemoveFile(@Param("data") data: string) {
        try {
            let removefile = await Upload.DeleteFile(data);
            return removefile;
        } catch(e) {
            console.log(e);
            throw new Error(e);
        }
    }

}