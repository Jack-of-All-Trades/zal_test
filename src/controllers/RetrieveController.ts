import { JsonController, Get, Param, OnUndefined} from 'routing-controllers';
import {Retrieve} from '../models/Retrieve';

@JsonController('/retrieve')

export class RetrieveController {

    @Get('/:data')
    @OnUndefined(404)
    async getOne(@Param("data") data: string){
        try {
            let getone = await Retrieve.GetOne(data);
            return getone;
        } catch(e) {
            console.log(e);
            throw new Error(e);
        }
    }
}