export interface IFile {
    firstname: string
    lastname: string
    age: number
    dob?: number
    mobile?: number
    description: string
    city?: string
}